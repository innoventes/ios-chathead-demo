//
//  ViewController.h
//  ChatHeads
//
//  Created by Matthias Hochgatterer on 4/19/13.
//  Copyright (c) 2013 Matthias Hochgatterer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CHDraggingCoordinator.h"

@interface ViewController : UIViewController<CHDraggingCoordinatorDelegate>
@property (strong, nonatomic) CHDraggingCoordinator *draggingCoordinator;

@end
