//
//  UIViewController+Chat.m
//  ChatHeads
//
//  Created by Sukanya D on 10/03/15.
//  Copyright (c) 2015 Matthias Hochgatterer. All rights reserved.
//

#import "Chat.h"

@implementation Chat

-(void)viewDidLoad{
    [super viewDidLoad];
    [self.chatText setDelegate:self];
    self.chatText.layer.borderWidth = 1.0f;
    self.chatText.layer.borderColor = [[UIColor grayColor] CGColor];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        NSLog(@"Return pressed, do whatever you like here");
        NSString *text = self.chatText.text;
        self.chatLable.text = text;
        [self.chatText resignFirstResponder];
        return NO; // or true, whetever you's like
    }
    
    return YES;
}

@end
