//
//  ViewController.m
//  ChatHeads
//
//  Created by Matthias Hochgatterer on 4/19/13.
//  Copyright (c) 2013 Matthias Hochgatterer. All rights reserved.
//

#import "ViewController.h"
#import "CHDraggableView.h"
#import "CHDraggableView+Avatar.h"
#import "Chat.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.title = @"Chat Heads";
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return 2;
            break;
        default:
            return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = [NSString stringWithFormat:@"User %ld",indexPath.row+1];
    NSInteger x = indexPath.row;
    if (x%2 == 0) {
    cell.imageView.image = [UIImage imageNamed:@"default_user_male.png"];
    }else{
        cell.imageView.image = [UIImage imageNamed:@"default_user_female.png"];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [[self.view.window viewWithTag:899] removeFromSuperview];
    CHDraggableView *draggableView;
    NSInteger x = indexPath.row;
    if (x%2 == 0) {
       draggableView = [CHDraggableView draggableViewWithImage:[UIImage imageNamed:@"default_user_male.png"]];
    }else{
        draggableView = [CHDraggableView draggableViewWithImage:[UIImage imageNamed:@"default_user_female.png"]];
    }
    draggableView.tag = 899;
    _draggingCoordinator = [[CHDraggingCoordinator alloc] initWithWindow:[[self view] window] draggableViewBounds:draggableView.bounds];
    _draggingCoordinator.delegate = self;
    _draggingCoordinator.snappingEdge = CHSnappingEdgeBoth;
    draggableView.delegate = _draggingCoordinator;
    [[[self view]window] addSubview:draggableView];
}

- (UIViewController *)draggingCoordinator:(CHDraggingCoordinator *)coordinator viewControllerForDraggableView:(CHDraggableView *)draggableView
{
    Chat *view = [[Chat alloc] initWithNibName:@"Chat" bundle:nil];
    return view;
}

@end
