//
//  UIViewController+Chat.h
//  ChatHeads
//
//  Created by Sukanya D on 10/03/15.
//  Copyright (c) 2015 Matthias Hochgatterer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Chat : UIViewController<UITextViewDelegate>
@property (strong, nonatomic) IBOutlet UILabel *chatLable;
@property (strong, nonatomic) IBOutlet UITextView *chatText;
@end
